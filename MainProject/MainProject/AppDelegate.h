//
//  AppDelegate.h
//  MainProject
//
//  Created by Vinoth on 13/02/18.
//  Copyright © 2018 Vinoth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

